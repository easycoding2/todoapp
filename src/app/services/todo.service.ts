import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiUrl = 'https://jsonplaceholder.typicode.com';
  constructor(private httpClient: HttpClient) {}
  public getPosts() {
    return this.httpClient.get(this.apiUrl + '/todos');
  }
}
