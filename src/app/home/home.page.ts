import { Component, OnInit } from '@angular/core';
import {TodoService} from '../services/todo.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public todos: any;
  filterTerm: string;
  constructor(public servis: TodoService) {
  }

  async ngOnInit() {
    this.servis.getPosts().subscribe((data3: any[]) => {
      this.todos = data3;
      console.log(data3);
    });
  }
}
